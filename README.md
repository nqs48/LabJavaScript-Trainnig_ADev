# LABORATORIO JAVASCRIPT POO  -- SOFKA
In this challenge we are going to model a question and answer contest, the intention is to design a solution that allows having a bank of questions with different options for a single answer, in addition each question must be in a category or a group of similar questions of the same level, for each round a prize must be assigned to get, The rounds of the game are levels that increase as the player wins prizes.



---

<!-- ABOUT THE PROJECT -->
## About The Project

Responsive design of a dynamic web page, where you can play a game of who wants to be millionaire, with your friends or family obtaining an undisputed winner.

- Key frontend and backend concepts
- JavaScript Vanilla
- Consumption of data from Local Storage

### [https://who-wants-qi.netlify.app/](https://who-wants-qi.netlify.app/)


---

# PROJECT REVIEW:


## CREATE GAME:

### Form to create game

Game creation with one player required.

![image](https://res.cloudinary.com/adev48/image/upload/v1658304017/Deployments/Who-Wants/init_fyfgfg.png)



---

## SELECT QUESTION:

### The correct option

Filter question to local storage with level.

![image](https://res.cloudinary.com/adev48/image/upload/v1658304017/Deployments/Who-Wants/selection_p3pypm.png)

---

## DETAILS GAME:

### Details of Rewards

Name gamer and rewards status.

![image](https://res.cloudinary.com/adev48/image/upload/v1658304017/Deployments/Who-Wants/ends_mnv2vd.png)

---

## Built With

This section contains the platforms that were used for the project.

* [JavaScript](https://developer.mozilla.org/es/docs/Web/JavaScript)
* [Git](https://git-scm.com/)
* [MongoDB Compass](https://www.mongodb.com/es/products/compass)


### Installation

Install each one the pieces of software previously mentioned.


1. Clone the repo

- HTTPS
   ```
   $ git clone https://github.com/nqs48/LabJavaScript-Trainnig_ADev.git
   ```


- SSH
   ```
   $ git clone git@github.com:nqs48/LabJavaScript-Trainnig_ADev.git
   ```


2. Open the project with VisualStudio Code (In the root proyect directory)

   ```
   $ open .
   ```
   

3. Open the localhost port 3000 in your preference browser

   ```
   http://localhost:3000/
   
   ```

---

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

---

<!-- CONTACT -->
## Collaborators
```
Nestor Quiroga Suarez
Jr. Software Developer
```
- LinkedIn => [Nestor Quiroga Suárez](https://www.linkedin.com/in/nqs48/)


Project Link: [https://who-wants-qi.netlify.app/](https://who-wants-qi.netlify.app/)

<p align="right">(<a href="#top">back to top</a>)</p>